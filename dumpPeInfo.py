#!/usr/bin/env python
import os
import sys
import struct
import hashlib
import os.path
import datetime
import subprocess

#CV_SIGNATURE_NB10 = '01BN'
CV_SIGNATURE_NB10 = 0x3031424e
#CV_SIGNATURE_RSDS = 'SDSR'
CV_SIGNATURE_RSDS = 0x53445352



try:
    import pefile
except ImportError:
    print "Could not import pefile. Install it from: http://code.google.com/p/pefile/"
    sys.exit(1)

def usage():
    print "python dumpPeTime.py <Pe_Files>"

def main():
    if len(sys.argv) <= 1:
        usage()
        return
        
    for filename in sys.argv[1:]:
        if not os.path.isfile(filename):
            print "%s is not a file!" % filename
            usage()
            return


        print "*"*78
        print "File information %s" % filename
        print "Size: %s" % addDecimalCommas(getFileSize(filename))
        print "Bytes: %s" % getFileSize(filename)
        print "MD5: %s" % getMd5HexHash(filename)
        handlePeStuff(filename)
        #print "PE Timestamp: %s" % getPeTimeString(filename)
        #print "%s - %s - %s" % (getPeTimeString(filename), getFileSize(filename), filename)
        doStrings(filename)
    return

def readRawString(pe, offset):
    if offset >= len(pe.__data__):
        raise RuntimeError("Bad offset")
    zeroOffset = pe.__data__.find('\x00', offset)
    if zeroOffset <0:
        raise RuntimeError("Couldn't find end of raw string")
    return pe.__data__[offset:zeroOffset]


def handleDebugInfo(pe):
    #http://www.debuginfo.com/examples/src/DebugDir.cpp
    if isUndefined(pe, 'DIRECTORY_ENTRY_DEBUG'):
        print "No debug data"
        return
    for dbgDir in pe.DIRECTORY_ENTRY_DEBUG:
        if isUndefined(dbgDir, 'struct'):
            print "Debug directory missing struct"
        else:
            dbgDataAddr = dbgDir.struct.AddressOfRawData
            signature = pe.get_dword_at_rva(dbgDataAddr)
            if (dbgDataAddr == 0) and (dbgDir.struct.PointerToRawData != 0):
                dbgOffset = dbgDir.struct.PointerToRawData
                print 'Tryinga raw data pointer: %08x' % dbgOffset
                signature = struct.unpack('<l', pe.__data__[dbgOffset:dbgOffset+4])[0]
            if signature == CV_SIGNATURE_RSDS:
                #offset 0x18 -> start of string for byte array
                if dbgDataAddr == 0:
                    pdbName = readRawString(pe, dbgDir.struct.PointerToRawData+0x18)
                else:
                    pdbName = pe.get_string_at_rva(dbgDataAddr+0x18)
                print 'PDB Path: "%s"' % trimNulls(pdbName)
            elif signature == CV_SIGNATURE_NB10:
                #print 'Found NB10 signatre'
                #offset 0x10 -> start of string for byte array
                if dbgDataAddr == 0:
                    pdbName = readRawString(pe, dbgDir.struct.PointerToRawData+0x10)
                else:
                    pdbName = pe.get_string_at_rva(dbgDataAddr+0x10)
                print 'PDB Path: "%s"' % trimNulls(pdbName)
            else:
                print "Unknown debug signature: %04x" % signature


def doStrings(filename):
#    strFilename = 'str_%s.txt' % filename
    print filename
    if "/" in filename:
        tmpFileName = filename.split('/')[-1]
        print tmpFileName
    elif "\\" in filename:
        tmpFileName = filename.split('\\')[-1]
        print tmpFileName
    else:
        tmpFileName = filename
    print tmpFileName
    strFilename = 'str_%s.txt' % (tmpFileName)
    print strFilename
    stringsFile = file(strFilename, 'w')
    print "Writings strings data to: %s" % strFilename
    p = subprocess.Popen( ['strings', '-a', filename], close_fds=True,  stdout=stringsFile)
    p.wait()
    p = subprocess.Popen( ['strings', '-a', '-e', 'l', filename], close_fds=True,  stdout=stringsFile)
    p.wait()
    stringsFile.close()


def trimNulls(inString):
    if inString is None:
        return ''
    idx = inString.find('\x00')
    if idx < 0:
        return inString
    return inString[:idx]

def handlePeStuff(filename):
    pe = None
    try:
        pe = pefile.PE(filename)
    except pefile.PEFormatError, err:
        #print "Skipping invalid filename: %s" % filename
        print "INVALID PE FILE"
        return

    print "Compile timestamp: %s" % getPeTimeString(pe)
    print "Section Names:"
    handleSectionNames(pe)
    print "Exports Data:"
    handleExportData(pe)
    handleDebugInfo(pe)

def getMd5HexHash(filename):
    inBytes = file(filename).read()
    m = hashlib.md5()
    m.update(inBytes)
    return m.hexdigest().upper()

def getFileSize(filename):
    return  os.path.getsize(filename)

def handleSectionNames(pe):
    retList = ["%s" % i.Name for i in pe.sections]
    if len(retList) == 0:
        return '    None!!!!'
    for s in retList:
        print '    "%s"' % trimNulls(s)

def isDefined(obj, name):
    return hasattr(obj, name) and (getattr(obj, name) is not None)

def isUndefined(obj, name):
    return not isDefined(obj, name)

def handleExportData(pe):
    #if not hasattr(pe, DIRECTORY_ENTRY_EXPORT) or pe.DIRECTORY_ENTRY_EXPORT is  None:
    if isUndefined(pe, 'DIRECTORY_ENTRY_EXPORT'):
        print 'No Export data'
        return

    #if not hasattr(pe.DIRECTORY_ENTRY_EXPORT, struct) or pe.DIRECTORY_ENTRY_EXPORT is not None:
    if isUndefined(pe.DIRECTORY_ENTRY_EXPORT, 'struct'):
        print 'No Export struct'
        return

    #if ((not hasattr(pe.DIRECTORY_ENTRY_EXPORT.struct, TimeDateStamp)) or
    #    (pe.DIRECTORY_ENTRY_EXPORT.struct.TimeDateStamp is None)):
    if isDefined(pe.DIRECTORY_ENTRY_EXPORT.struct, 'TimeDateStamp'):
        timeDateStamp = pe.DIRECTORY_ENTRY_EXPORT.struct.TimeDateStamp
        dt = datetime.datetime.utcfromtimestamp(timeDateStamp) 
        #dateStr = dt.strftime("%Y-%m-%d %H:%M:%S UTC")
        dateStr = dt.strftime("%Y-%m-%d %H:%M:%SZ")
        print  "Export datetime (0x%08x): %s" % (timeDateStamp, dateStr)
    else:
        print "No export datetime"
    if isDefined(pe.DIRECTORY_ENTRY_EXPORT.struct, 'Name'):
        exportName = pe.DIRECTORY_ENTRY_EXPORT.struct.Name
        if isinstance(exportName, int):
            print 'Export filename: "%s"' % pe.get_string_at_rva(exportName)
        else:
            print "Don't know how to handle export name: %s" % str(type(exportName))
    else:
        print "No export filename"

    if isDefined(pe.DIRECTORY_ENTRY_EXPORT, 'symbols'):
        print "Export symbols:"
        for s in pe.DIRECTORY_ENTRY_EXPORT.symbols:
            print '    0x%04x (%5d): "%s"' % (s.ordinal, s.ordinal, trimNulls(s.name))
    else:
        print "No Export symbols"

def getPeTimeString(pe):
    try:
        #pe = pefile.PE(filename, fast_load=True)
        timeDateStamp = pe.NT_HEADERS.FILE_HEADER.TimeDateStamp

        dt = datetime.datetime.utcfromtimestamp(timeDateStamp) 
        #dateStr = dt.strftime("%Y-%m-%d %H:%M:%S UTC")
        dateStr = dt.strftime("%Y-%m-%d %H:%M:%SZ")
        return "(0x%08x): %s" % (timeDateStamp, dateStr)
    except pefile.PEFormatError, err:
        pass
        #print "Skipping invalid filename: %s" % filename
        return "INVALID PE FILE"

def addDecimalCommas(val):
    val = "%d" % val
    retArr = []
    for i in range(len(val)/3):
        retArr.append( val[ len(val)-3*(i+1):len(val)-3*i] )
    if len(val) % 3 != 0:
        retArr.append( val[:len(val) % 3])
    retArr = retArr[::-1]
    return ','.join(retArr)


if __name__== '__main__':
    main()
